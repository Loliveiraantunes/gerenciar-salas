package br.com.container.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Equipamento.class)
public abstract class Equipamento_ {

	public static volatile SingularAttribute<Equipamento, String> nomeEquipamento;
	public static volatile SingularAttribute<Equipamento, String> localidade;
	public static volatile SingularAttribute<Equipamento, Long> patrimonio;
	public static volatile SingularAttribute<Equipamento, Long> id;
	public static volatile SingularAttribute<Equipamento, Date> cadastro;
	public static volatile SingularAttribute<Equipamento, String> descricao;

}

