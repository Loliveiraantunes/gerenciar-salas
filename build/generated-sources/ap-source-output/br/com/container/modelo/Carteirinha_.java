package br.com.container.modelo;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Carteirinha.class)
public abstract class Carteirinha_ {

	public static volatile SingularAttribute<Carteirinha, Aluno> aluno;
	public static volatile SingularAttribute<Carteirinha, String> numero;
	public static volatile SingularAttribute<Carteirinha, Curso> curso;
	public static volatile SingularAttribute<Carteirinha, Long> id;
	public static volatile SingularAttribute<Carteirinha, Date> validade;

}

