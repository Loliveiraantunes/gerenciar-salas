package br.com.container.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Empresa.class)
public abstract class Empresa_ {

	public static volatile SingularAttribute<Empresa, String> telefone;
	public static volatile SingularAttribute<Empresa, String> nomeEmpresa;
	public static volatile SingularAttribute<Empresa, Endereco> endereco;
	public static volatile SingularAttribute<Empresa, String> nomeContato;
	public static volatile SingularAttribute<Empresa, Long> id;

}

