/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.container.controle;

import br.com.container.dao.EquipamentoDao;
import br.com.container.dao.EquipamentoDaoImp;
import br.com.container.dao.HibernateUtil;
import br.com.container.modelo.Equipamento;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author loliv
 */
@ManagedBean(name = "equipamentoC")
@ViewScoped
public class EquipamentoControle {

    private Equipamento equipamento;
    private EquipamentoDao equipamentoDao;
    private Session sessao;
    private DataModel<Equipamento> dataModel;
    private List<Equipamento> equipamentos;
    private boolean mostra_toolbar;

    private void abreSessao() {
        if (sessao == null) {
            sessao = HibernateUtil.abreSessao();
        } else if (!sessao.isOpen()) {
            sessao = HibernateUtil.abreSessao();
        }
    }

    public EquipamentoControle() {
        equipamento = new Equipamento();
        equipamentoDao = new EquipamentoDaoImp();
    }

    public void novo() {
        equipamento = new Equipamento();
        mostra_toolbar = !mostra_toolbar;
    }

    public void novaPesquisa() {
        equipamento = new Equipamento();
        mostra_toolbar = !mostra_toolbar;
    }

    public void preparaAlterar() {

        mostra_toolbar = !mostra_toolbar;
    }

    public void pesquisar() {
        equipamentoDao = new EquipamentoDaoImp();
        try {
            abreSessao();
            equipamentos = equipamentoDao.pesquisaPorNome(equipamento.getNomeEquipamento(), sessao);
            dataModel = new ListDataModel(equipamentos);
            equipamento.setNomeEquipamento(null);
        } catch (Exception e) {
            System.out.println("erro ao pesquisar Equipamento por nome: " + e.getMessage());
        } finally {
            sessao.close();
        }
    }

    public void excluir() {
        equipamento = dataModel.getRowData();
        equipamentoDao = new EquipamentoDaoImp();
        abreSessao();
        try {
            equipamentoDao.remover(equipamento, sessao);
            equipamentos.remove(equipamento);
            dataModel = new ListDataModel(equipamentos);
            Mensagem.excluir("Equipamento");
            limpar();
        } catch (Exception e) {
            System.out.println("erro ao excluir: " + e.getMessage());
        } finally {
            sessao.close();
        }
    }

    public void salvar() {

        equipamentoDao = new EquipamentoDaoImp();
        abreSessao();
        try {
            equipamentoDao.salvarOuAlterar(equipamento, sessao);
            Mensagem.salvar("Equipamento " + equipamento.getNomeEquipamento());
            equipamento = null;
        } catch (HibernateException e) {
            System.out.println("Erro ao salvar empresa " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Erro no salvar empresaDao Controle "
                    + e.getMessage());
        } finally {
            sessao.close();
        }
    }

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public EquipamentoDao getEquipamentoDao() {
        return equipamentoDao;
    }

    public void setEquipamentoDao(EquipamentoDao equipamentoDao) {
        this.equipamentoDao = equipamentoDao;
    }

    public Session getSessao() {
        return sessao;
    }

    public void setSessao(Session sessao) {
        this.sessao = sessao;
    }

    public DataModel<Equipamento> getDataModel() {
        return dataModel;
    }

    public void setDataModel(DataModel<Equipamento> dataModel) {
        this.dataModel = dataModel;
    }

    public List<Equipamento> getEquipamentos() {
        return equipamentos;
    }

    public void setEquipamentos(List<Equipamento> equipamentos) {
        this.equipamentos = equipamentos;
    }

    public boolean isMostra_toolbar() {
        return mostra_toolbar;
    }

    public void setMostra_toolbar(boolean mostra_toolbar) {
        this.mostra_toolbar = mostra_toolbar;
    }

    public void limpar() {
        equipamento = new Equipamento();
    }

}
