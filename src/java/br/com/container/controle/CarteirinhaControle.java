/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.container.controle;

import br.com.container.dao.AlunoDao;
import br.com.container.dao.AlunoDaoImp;
import br.com.container.dao.CarteirinhaDao;
import br.com.container.dao.CarteirinhaDaoImpl;
import br.com.container.dao.CursoDao;
import br.com.container.dao.CursoDaoImpl;
import br.com.container.dao.HibernateUtil;
import br.com.container.modelo.Aluno;
import br.com.container.modelo.Carteirinha;
import br.com.container.modelo.Curso;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author Carteirinha
 */
@ManagedBean(name = "carteirinhaC")
@ViewScoped
public class CarteirinhaControle implements Serializable {

    private boolean mostraToolbar = false;
    private String pesqNome = "";

    private Session session;
    private CarteirinhaDao dao;
    private Carteirinha carteirinha;
    private Aluno aluno;
    private List<Aluno> alunos;
    private  AlunoDao alunoDao ;

    private Curso curso;
    private List<Curso> cursos;
    private CursoDao cursoDao;
    
    
    private List<Carteirinha> carteirinhas;
    private DataModel<Carteirinha> modelCarteirinhas;

    public CarteirinhaControle() {
        dao = new CarteirinhaDaoImpl();
        carteirinha = new Carteirinha();
        
        alunoDao = new AlunoDaoImp();
        aluno = new Aluno();
        alunos = new ArrayList<>();
        
        cursoDao = new CursoDaoImpl();
        curso = new Curso();
        cursos = new ArrayList<>();
    }

    public List<Aluno> pesquisaAluno(String query) {
        abreSessao();
        alunoDao = new AlunoDaoImp();
        try {
            alunos = alunoDao.pesquisaPorNome(query, session);
        } catch (HibernateException he) {
            System.out.println("Erro no pesquisaAlunos()" + he.getMessage());
        } finally {
            session.close();
        }
        return alunos;
    }

    
     public List<Curso> pesquisaCurso(String query) {
        abreSessao();
        cursoDao = new CursoDaoImpl();
        try {
          cursos = cursoDao.pesquisaPorNome(query, session);
        } catch (HibernateException he) {
            System.out.println("Erro no pesquisaCurso()" + he.getMessage());
        } finally {
            session.close();
        }
        return cursos;
    }

    
    public void pesquisar() {
        dao = new CarteirinhaDaoImpl();
        carteirinhas = new ArrayList<>();
        try {
            abreSessao();
            if (!pesqNome.equals("")) {
                carteirinhas = dao.pesquisaPorNome(pesqNome, session);
            } else {
                carteirinhas = dao.listaTodos(session);
            }
            modelCarteirinhas = new ListDataModel(carteirinhas);
            pesqNome = null;
        } catch (HibernateException ex) {
            System.err.println("Erro pesquisa carteirinha:\n" + ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void salvar() {
        dao = new CarteirinhaDaoImpl();
        carteirinha.setCurso(curso);
        carteirinha.setAluno(aluno);
        try {
            abreSessao();
            dao.salvarOuAlterar(carteirinha, session);
            Mensagem.salvar("Carteirinha " + carteirinha.getNumero());
        } catch (Exception ex) {
            Mensagem.mensagemError("Erro ao salvar\nTente novamente");
            System.err.println("Erro pesquisa carteirinha:\n" + ex.getMessage());
        } finally {
            carteirinha = new Carteirinha();
            session.close();
        }
    }

    public void alterarAluno() {
        mostraToolbar = !mostraToolbar;
        carteirinha = modelCarteirinhas.getRowData();
    }

    public void excluir() {
        carteirinha = modelCarteirinhas.getRowData();
        dao = new CarteirinhaDaoImpl();
        try {
            abreSessao();
            dao.remover(carteirinha, session);
            Mensagem.excluir("Carteirinha " + carteirinha.getNumero());
            carteirinhas.remove(carteirinha);
            modelCarteirinhas = new ListDataModel(carteirinhas);
            carteirinha = new Carteirinha();
        } catch (Exception ex) {
            System.err.println("Erro ao excluir carteirinha:\n" + ex.getMessage());
        } finally {
            session.close();
        }
    }

    private void abreSessao() {
        if (session == null || !session.isOpen()) {
            session = HibernateUtil.abreSessao();
        }
    }

    public void mudaToolbar() {
        carteirinha = new Carteirinha();
        carteirinhas = new ArrayList();
        pesqNome = "";
        mostraToolbar = !mostraToolbar;
    }

    public boolean isMostraToolbar() {
        return mostraToolbar;
    }

    public void setMostraToolbar(boolean mostraToolbar) {
        this.mostraToolbar = mostraToolbar;
    }

    public String getPesqNome() {
        return pesqNome;
    }

    public void setPesqNome(String pesqNome) {
        this.pesqNome = pesqNome;
    }

    public AlunoDao getAlunoDao() {
        return alunoDao;
    }

    public void setAlunoDao(AlunoDao alunoDao) {
        this.alunoDao = alunoDao;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public CursoDao getCursoDao() {
        return cursoDao;
    }

    public void setCursoDao(CursoDao cursoDao) {
        this.cursoDao = cursoDao;
    }

    
    
    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public List<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<Aluno> alunos) {
        this.alunos = alunos;
    }
    
    

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public CarteirinhaDao getDao() {
        return dao;
    }

    public void setDao(CarteirinhaDao dao) {
        this.dao = dao;
    }

    public Carteirinha getCarteirinha() {
        return carteirinha;
    }

    public void setCarteirinha(Carteirinha carteirinha) {
        this.carteirinha = carteirinha;
    }

    public List<Carteirinha> getCarteirinhas() {
        return carteirinhas;
    }

    public void setCarteirinhas(List<Carteirinha> carteirinhas) {
        this.carteirinhas = carteirinhas;
    }

    public DataModel<Carteirinha> getModelCarteirinhas() {
        return modelCarteirinhas;
    }

    public void setModelCarteirinhas(DataModel<Carteirinha> modelCarteirinhas) {
        this.modelCarteirinhas = modelCarteirinhas;
    }

}
