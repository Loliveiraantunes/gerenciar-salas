/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.container.controle;

import br.com.container.dao.EmpresaDao;
import br.com.container.dao.EmpresaDaoImp;
import br.com.container.dao.HibernateUtil;
import br.com.container.modelo.Empresa;
import br.com.container.modelo.Endereco;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 *
 * @author loliv
 */
@ManagedBean(name = "empresaC")
@ViewScoped
public class EmpresaControle {

    private Empresa empresa;
    private Endereco endereco;
    private EmpresaDao empresaDao;
    private Session sessao;
    private DataModel<Empresa> modelEmpresa;
    private List<Empresa> empresas;
    private List<SelectItem> funcoes;
    private boolean mostra_toolbar;

    private void abreSessao() {
        if (sessao == null) {
            sessao = HibernateUtil.abreSessao();
        } else if (!sessao.isOpen()) {
            sessao = HibernateUtil.abreSessao();
        }
    }

    public EmpresaControle() {
        empresa = new Empresa();
        endereco = new Endereco();
    }

    public void novo() {
        empresa = new Empresa();
        endereco = new Endereco();
        mostra_toolbar = !mostra_toolbar;
    }

    public void novaPesquisa() {
        empresa = new Empresa();
        endereco = new Endereco();
        mostra_toolbar = !mostra_toolbar;
    }

    public void preparaAlterar() {
        mostra_toolbar = !mostra_toolbar;
    }

    public void pesquisar() {
        empresaDao = new EmpresaDaoImp();
        try {
            abreSessao();
            empresas = empresaDao.pesquisaPorNome(empresa.getNomeEmpresa(), sessao);
            modelEmpresa = new ListDataModel(empresas);
            empresa.setNomeEmpresa(null);
        } catch (Exception e) {
            System.out.println("erro ao pesquisar Empresa por nome: " + e.getMessage());
        } finally {
            sessao.close();
        }
    }

    public void excluir() {
        empresa = modelEmpresa.getRowData();
        empresaDao = new EmpresaDaoImp();
        abreSessao();
        try {
            empresaDao.remover(empresa, sessao);
            empresas.remove(empresa);
            modelEmpresa = new ListDataModel(empresas);
            Mensagem.excluir("Empresa");
            limpar();
        } catch (Exception e) {
            System.out.println("erro ao excluir: " + e.getMessage());
        } finally {
            sessao.close();
        }
    }

    public void salvar() {

        empresa.setEndereco(endereco);
        endereco.setEmpresa(empresa);
        empresaDao = new EmpresaDaoImp();
        abreSessao();
        try {

            empresaDao.salvarOuAlterar(empresa, sessao);
            Mensagem.salvar("Empresa " + empresa.getNomeEmpresa());
            empresa = null;
            endereco = null;
        } catch (HibernateException e) {
            System.out.println("Erro ao salvar empresa " + e.getMessage());
        } catch (Exception e) {
            System.out.println("Erro no salvar empresaDao Controle "
                    + e.getMessage());
        } finally {
            sessao.close();
        }
    }

    public void limpar() {
        empresa = new Empresa();
        endereco = new Endereco();
    }

    public void carregarParaAlterar() {
        mostra_toolbar = !mostra_toolbar;
        empresa = modelEmpresa.getRowData();
        endereco = empresa.getEndereco();
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public EmpresaDao getEmpresaDao() {
        return empresaDao;
    }

    public void setEmpresaDao(EmpresaDao empresaDao) {
        this.empresaDao = empresaDao;
    }

    public Session getSessao() {
        return sessao;
    }

    public void setSessao(Session sessao) {
        this.sessao = sessao;
    }

    public DataModel<Empresa> getModelEmpresa() {
        return modelEmpresa;
    }

    public void setModelEmpresa(DataModel<Empresa> modelEmpresa) {
        this.modelEmpresa = modelEmpresa;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }

    public List<SelectItem> getFuncoes() {
        return funcoes;
    }

    public void setFuncoes(List<SelectItem> funcoes) {
        this.funcoes = funcoes;
    }

    public boolean isMostra_toolbar() {
        return mostra_toolbar;
    }

    public void setMostra_toolbar(boolean mostra_toolbar) {
        this.mostra_toolbar = mostra_toolbar;
    }

}
