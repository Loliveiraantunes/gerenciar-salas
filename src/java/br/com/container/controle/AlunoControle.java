package br.com.container.controle;

import br.com.container.dao.AlunoDao;
import br.com.container.dao.AlunoDaoImp;
import br.com.container.dao.HibernateUtil;
import br.com.container.modelo.Aluno;
import br.com.container.modelo.Endereco;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import org.hibernate.HibernateException;
import org.hibernate.Session;

@ManagedBean(name = "alunoC")
@ViewScoped
public class AlunoControle implements Serializable {

    private boolean mostraToolbar = false;
    private String pesqNome = "";

    private Session session;
    private AlunoDao dao;

    private Endereco endereco;
    private Aluno aluno;
    private List<Aluno> alunos;
    private DataModel<Aluno> modelAlunos;

    public AlunoControle() {
         dao = new AlunoDaoImp();
    }

    public void pesquisar() {
        dao = new AlunoDaoImp();
        try {
            abreSessao();
            if (!pesqNome.equals("")) {
                alunos = dao.pesquisaPorNome(pesqNome, session);
            } else {
                alunos = dao.listaTodos(session);
            }

            modelAlunos = new ListDataModel(alunos);
            pesqNome = null;
        } catch (HibernateException ex) {
            System.err.println("Erro pesquisa aluno:\n" + ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void salvar() {
        dao = new AlunoDaoImp();
        try {
            abreSessao();
            aluno.setEndereco(endereco);
            endereco.setPessoa(aluno);
            dao.salvarOuAlterar(aluno, session);
            Mensagem.salvar("Aluno " + aluno.getNome());
        } catch (Exception ex) {
            Mensagem.mensagemError("Erro ao salvar\nTente novamente");
            System.err.println("Erro pesquisa aluno:\n" + ex.getMessage());
        } finally {
            aluno = new Aluno();
            endereco = new Endereco();
            session.close();
        }
    }
    
    
    public void alterarAluno() {
        mostraToolbar = !mostraToolbar;
        aluno = modelAlunos.getRowData();
        endereco = aluno.getEndereco();
    }

    
      public void excluir() {
        aluno = modelAlunos.getRowData();
        dao = new AlunoDaoImp();
        try {
            abreSessao();
            dao.remover(aluno, session);
            Mensagem.excluir("Aluno " + aluno.getNome());
            alunos.remove(aluno);
            modelAlunos = new ListDataModel(alunos);
            aluno = new Aluno();
            endereco = new Endereco();
        } catch (Exception ex) {
            System.err.println("Erro ao excluir aluno:\n" + ex.getMessage());
        } finally {
            session.close();
        }
    }
    
    
    private void abreSessao() {
        if (session == null || !session.isOpen()) {
            session = HibernateUtil.abreSessao();
        }
    }

    public void mudaToolbar() {
        aluno = new Aluno();
        endereco = new Endereco();
        alunos = new ArrayList();
        pesqNome = "";
        mostraToolbar = !mostraToolbar;
    }

    public boolean isMostraToolbar() {
        return mostraToolbar;
    }

    public DataModel<Aluno> getModelAlunos() {
        return modelAlunos;
    }

    public void setModelAlunos(DataModel<Aluno> modelAlunos) {
        this.modelAlunos = modelAlunos;
    }

    public void setMostraToolbar(boolean mostraToolbar) {
        this.mostraToolbar = mostraToolbar;
    }

    public String getPesqNome() {
        return pesqNome;
    }

    public void setPesqNome(String pesqNome) {
        this.pesqNome = pesqNome;
    }


    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

 
}
